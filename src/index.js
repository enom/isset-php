/**
 * Safe and simple PHP isset() for JavaScript.
 *
 * Determine if a variable is considered set, this means if a variable is
 * declared and is different than null or undefined.
 *
 * If a variable has been unset with the delete keyword, it is no longer
 * considered to be set.
 *
 * isset() will return false when checking a variable that has been assigned to
 * null or undefined. Also note that a null character ("\0") is not equivalent
 * to the JavaScript null constant.
 *
 * If multiple parameters are supplied then isset() will return true only if
 * all of the parameters are considered set. Evaluation goes from left to right
 * and stops as soon as an unset variable is encountered.
 *
 * @param {Function} accessor Accessor functions returning the variable to be checked
 * @param {...Function} accessors Further accessors functions
 * @returns {Boolean} False if any accessor returns null or undefined
 */
module.exports = function isset () {
  if (!arguments.length) throw new TypeError('isset requires at least one accessor function')
  return Array.prototype.slice.call(arguments).every((accessor) => {
    if (typeof accessor !== 'function') throw new TypeError('isset requires accessors to be functions')
    try {
      const value = accessor()
      return value !== undefined && value !== null
    } catch (e) {
      return false
    }
  })
}
